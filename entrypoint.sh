#!/bin/sh

# Exit if any command fails
set -e

# If DEBUG is not set
if [ -z ${DEBUG} ]
then
    # Start the production server
    exec uwsgi uwsgi.ini
else
    # Start the development server
    exec python main.py
fi