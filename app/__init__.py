from flask import Flask

from . import config, routes


def create_app(config=config.Config):
    app = Flask(__name__)
    app.config.from_object(config)

    app.register_blueprint(routes.bp)

    return app
