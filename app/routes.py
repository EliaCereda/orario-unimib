from flask import Blueprint, current_app as app, Response
import requests
import re

from .errors import bad_request, service_unavailable


bp = Blueprint('api', __name__)


def process_calendar(calendar):
    # Split the DESCRIPTION and LOCATION fields
    calendar = re.sub(
        r"""
            ^DESCRIPTION:
            (.*)
            \s
            (U[\w\d-]+(\s\[.*\])?)
            \s
            (.*)$
        """, r"DESCRIPTION:\1 \4\nLOCATION:\2", calendar, flags=re.MULTILINE | re.VERBOSE)

    calendar = calendar.replace(',', '\,')

    return calendar


@bp.route('/')
def get_calendar():
    try:
        url = app.config['UPSTREAM_CALENDAR_URL']
        calendar = requests.get(url).text
        calendar = process_calendar(calendar)

        return Response(calendar, mimetype='text/plain')
    except requests.ConnectionError:
         return service_unavailable('Unable to connect to the server.')

@bp.route('/raw')
def get_raw_calendar():
    try:
        url = app.config['UPSTREAM_CALENDAR_URL']
        calendar = requests.get(url).text

        return Response(calendar, mimetype='text/plain')
    except requests.ConnectionError:
         return service_unavailable('Unable to connect to the server.')
